'use strict'
const express = require('express')
const app = express()                    //function express

const port = 8000

app.get('/movies', (req, res) => {                 // get(path,) if it have http get method come go to this function
    const results = {
        results: [{
            title: 'fake title 1',
            image_url: 'url1',
            overview: 'overview1'
        }, {
            title: 'fake title 2',
            image_url: 'url2',
            overview: 'overview2'
        }]
    }
    res.json(results)
})



app.listen(port, () => {                      //call back listen and then do
    console.log(`started at ${port}`)
})                            